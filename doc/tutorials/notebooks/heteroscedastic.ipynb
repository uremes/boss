{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "5bc9aac6",
   "metadata": {},
   "source": [
    "# Heteroscedastic noise"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e7b841a",
   "metadata": {},
   "source": [
    "This tutorial demonstrates how to treat heteroscedastic noise problems with BOSS. Heteroscedasticity implies that the noise can vary across the search space [1]. Heteroscedastic models are useful in cases where the objective funtion is known to be heteroscedastic."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "63bb3f3e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from boss.bo.bo_main import BOMain"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2d1be9e4",
   "metadata": {},
   "source": [
    "In this tutorial, we will minimize the one-dimensional Rastrigin function $f(x)=10+x^2-10\\cos(2\\pi x)$ in the interval $-5.12 \\le x \\le 5.12$. To make the function heteroscedastic, we add some random noise to it. This is achieved by sampling a value, `r`, from a normal distribution with a mean of zero and a standard deviation of `c`. The value `r`, multiplied by the true, noiseless value of the objective function, gives the noise at a given output value of the objective function. In this example, we set `c` to $0.1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7266fb43",
   "metadata": {
    "lines_to_end_of_cell_marker": 2
   },
   "outputs": [],
   "source": [
    "c = 0.1\n",
    "\n",
    "\n",
    "def func(X):\n",
    "    x = X[0, 0]\n",
    "    y = 10.0 + x**2 - 10 * np.cos(2 * np.pi * x)  # Ground truth value\n",
    "    r = np.random.normal(scale=c)\n",
    "    return y * (1.0 + r)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e3b4fa01",
   "metadata": {},
   "source": [
    "Next, we present two examples to demonstrate the use of heteroscedastic models."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "147ef032",
   "metadata": {},
   "source": [
    "### Example 1: Simple noise function"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2773acbf",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "source": [
    "A heteroscedastic model requires a noise estimation function, which must be defined as a Python function. The noise function is used by the model to estimate noise associated with samples of the objective function. In this example, the function estimates the noise at $x$ to be $10\\,\\%$ of the predicted function value $f(x)$. Note that the noise function should output variance values.\n",
    "\n",
    "The arguments of the noise function are always the following:\n",
    "* `hsc_args` (list) passes additional, user-given arguments to the noise function.\n",
    "* `**kwargs` (dict) can be used to access the model data/parameters. It contains the sampled values (`Y`) and locations (`X`), the GP surrogate (`model`) and the kernel lengthscales (`lengthscale`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f4bb0874",
   "metadata": {},
   "outputs": [],
   "source": [
    "def noise_func1(hsc_args, **kwargs):\n",
    "    mu_array, _ = kwargs[\"model\"].predict(kwargs[\"X\"])\n",
    "    noise_array = hsc_args[0] * np.abs(mu_array)\n",
    "    # Noise array must contain noise variance values,\n",
    "    # not standard deviation values\n",
    "    noise_array = [i**2 for i in noise_array]\n",
    "    return noise_array\n",
    "\n",
    "\n",
    "hsc_args = [0.1]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c177ecbb",
   "metadata": {},
   "source": [
    "Next, we create a `BOMain` instance and perform the optimization with `model_name=hsc` to use a heteroscedastic model. The noise function is passed to BOSS using the `hsc_noise` keyword and the list of user-given arguments using the `hsc_args` keyword."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e1339923",
   "metadata": {},
   "outputs": [],
   "source": [
    "bo = BOMain(\n",
    "    func,\n",
    "    bounds=[-5.12, 5.12],\n",
    "    model_name='hsc',\n",
    "    yrange=[0., 50.],\n",
    "    initpts=2,\n",
    "    iterpts=20,\n",
    "    hsc_noise=noise_func1,\n",
    "    hsc_args=hsc_args,\n",
    "    seed=25,\n",
    ")\n",
    "\n",
    "res = bo.run()\n",
    "\n",
    "print(\"Predicted global min: \", res.select(\"mu_glmin\", -1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6d0364d",
   "metadata": {},
   "source": [
    "For comparison, we can do the same optimization with the standard homoscedastic model. For this, we need to choose a value for the noise hyperparameter. Let's choose it to be the mean noise of the objective function, which is about 4.49."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c5d77c0a",
   "metadata": {},
   "outputs": [],
   "source": [
    "bo = BOMain(\n",
    "    func,\n",
    "    bounds=[-5.12, 5.12],\n",
    "    yrange=[0.0, 50.0],\n",
    "    initpts=3,\n",
    "    iterpts=20,\n",
    "    noise=4.49,\n",
    "    seed=25,\n",
    ")\n",
    "\n",
    "res = bo.run()\n",
    "\n",
    "print(\"Predicted global min: \", res.select(\"mu_glmin\", -1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3da897cc",
   "metadata": {},
   "source": [
    "The true value of the global minimum is zero. In this case, the heteroscedastic model is clearly better. This is usually the case if the objective function is heteroscedastic. However, if the noise hyperparameter given to the homoscedastic model corresponds well to the noise of the objective function around the global minimum, the models often perform very similarly. The homoscedastic model should be used in cases where it is sufficient, since the heteroscedastic model is computationally heavier to run."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f472e371",
   "metadata": {},
   "source": [
    "### Example 2: Known noise of initial data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aed62453",
   "metadata": {},
   "source": [
    "In some cases, the user might know the noise of the initial data. In such a case, it is possible to create a noise estimation function that takes this into account. In this example, we modify the previous noise function so that the noise values of the initial data are always set to the same, user-defined values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aa5395d1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Initial data points and noise\n",
    "X_init = [-5, 0, 2, 4]\n",
    "Y_init = [25, 0, 4, 16]\n",
    "noise_init = [6.25, 0.0, 0.16, 2.56]\n",
    "\n",
    "\n",
    "def noise_func2(hsc_args, **kwargs):\n",
    "    mu_array, _ = kwargs[\"model\"].predict(kwargs[\"X\"])\n",
    "    noise_array = hsc_args[0] * np.abs(mu_array)\n",
    "    # Noise array must contain noise variance values,\n",
    "    # not standard deviation values\n",
    "    noise_array = [i**2 for i in noise_array]\n",
    "    # Set the noise of inital data to known values\n",
    "    noise_array[:4] = np.atleast_2d(noise_init).T\n",
    "    return noise_array\n",
    "\n",
    "\n",
    "hsc_args = [0.1]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b144252",
   "metadata": {},
   "source": [
    "The heteroscedastic model requires noise estimates of the initial data points for the first optimization of hyperparameters during the model initialization step. If the noise of the initial data is known, the data can be passed to the model as a list using the ``noise_init`` keyword. If initial noise estimates are not specified, the default value of ``10e-12`` is used for all initial data points. After the model initialization, the noise function is used to estimate noise, even for the initial data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f9d7c1f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "bo = BOMain(\n",
    "    func,\n",
    "    bounds=[-5.12, 5.12],\n",
    "    model_name='hsc',\n",
    "    yrange=[0., 50.],\n",
    "    initpts=4,\n",
    "    iterpts=20,\n",
    "    hsc_noise=noise_func2,\n",
    "    noise_init=noise_init,  # only used during model initialization\n",
    "    hsc_args=[0.1],\n",
    "    seed=25,\n",
    ")\n",
    "\n",
    "res = bo.run(X_init=X_init, Y_init=Y_init)\n",
    "\n",
    "print(\"Predicted global min: \", res.select(\"mu_glmin\", -1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7ce7c8f7",
   "metadata": {},
   "source": [
    "[1] https://en.wikipedia.org/wiki/Homoscedasticity_and_heteroscedasticity"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "base",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}