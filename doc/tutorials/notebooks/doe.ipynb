{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "470d8623",
   "metadata": {
    "lines_to_next_cell": 2,
    "region_name": "md"
   },
   "source": [
    "# Design of experiments with BOSS\n",
    "This notebook demonstrates a simple workflow for performing design of experiments (DOE)\n",
    "with the help of BOSS. To illustrate the process we will use data from the paper\n",
    "[Machine Learning Optimization of Lignin Properties in Green Biorefineries](https://pubs.acs.org/doi/full/10.1021/acssuschemeng.2c01895).\n",
    "Here, the goal is to maximize the yield of lignin extracted from birch wood. The yield is a function\n",
    "of two input variables, the P-factor (quantifying the reaction severity) and the reactor temperature.\n",
    "The P-factor can range between 500 and 2500 while the temperature ranges between 180-210 Celsius.\n",
    "To keep things simple we will store and load data using pandas."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a5d13d3b",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "# Import necessary modules\n",
    "import pandas as pd\n",
    "from boss.bo.bo_main import BOMain\n",
    "import numpy as np\n",
    "from boss.bo.initmanager import InitManager\n",
    "\n",
    "# Define variable names + bounds\n",
    "inputs = ['p_fac', 'temp']\n",
    "output = ['yield']\n",
    "bounds = np.array([[500., 2500.], [180., 210.]]) "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01adb169",
   "metadata": {
    "region_name": "md"
   },
   "source": [
    "## Step 1: Collect initial data (optional if you already have data)\n",
    "To initialize the Gaussian process model used in the BO, we must have access to a small set of data points\n",
    "(for a 2D problem we might use 4 to 12 initial points). If no data has been collected yet, we can generate\n",
    "a set of suggested input variables using a Sobol sequence, for easy retrieval and visualization we store \n",
    "the data in a pandas dataframe that can easily be exported to various formats."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ef4d180a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate initial data \n",
    "im = InitManager(inittype='sobol', bounds=bounds, initpts=8)\n",
    "X_init = im.get_all()\n",
    "\n",
    "# store data in dataframe\n",
    "df = pd.DataFrame({'p_fac': X_init[:, 0], 'temp': X_init[:, 1], 'yield': np.nan})\n",
    "\n",
    "# inspect data and save to CSV file\n",
    "print(df)\n",
    "df.to_csv('lignin_data.csv', index=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d4975ae7",
   "metadata": {
    "region_name": "md"
   },
   "source": [
    "Note how the yield column is empty, we must now measure the yield for the input variables \n",
    "suggested in the dataframe and update the corresponding entries. Let's pretend we did \n",
    "this for our lignin problem and got the following results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c5ba1df3",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "measured_yields = [40.0, 70.3, 91.9, 51.0, 67.8, 84.8, 84.4, 45.3]\n",
    "# Normally performing the experiments took some time so we would need to read back our inital data from disk\n",
    "df = pd.read_csv('lignin_data.csv', index_col=None)\n",
    "df.loc[:, 'yield'] = measured_yields"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9685817",
   "metadata": {
    "region_name": "md"
   },
   "source": [
    "## Step 2: Run one BOSS iteration (this part must be ran many times)\n",
    "At this point we have some initial data available and can now perform one iteration of Bayesian optimization.\n",
    "Note that since experiments must be physically performed we can only do one BO iteration at a time. Running the code\n",
    "in this section of the notebook thus amounts to doing a single iteration of BO, resulting in a single new set of suggested input variables\n",
    "(or many sets if you use batch acquisitions) for which an experiment must be performed and the data updated manually."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1eaa4977",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract the data we want from the dataframe into X and Y arrays that can be passed to BOSS.\n",
    "# Recall that BOSS will try to minimize the output variable by default, hence to \n",
    "# maximize yield we need to insert a minus sign in front of the y-data.\n",
    "X = df.loc[:, inputs].to_numpy()\n",
    "Y = -df.loc[:, output].to_numpy()\n",
    "\n",
    "def f_dummy(X):\n",
    "    \"\"\"BOSS requires an objective function coded in python to work,\n",
    "    but since our objective is a physical experiment we just leave\n",
    "    the function empty here and raise an error if it is called.\n",
    "    \"\"\"\n",
    "    raise NotImplementedError\n",
    "\n",
    "# Below are some fairly typical settings for when BOSS is used for experiment design.\n",
    "# Note that you need to set the noise parameter, which should be an estimate\n",
    "# of the variance (not the standard deviation!) in the measurement of the output variable.\n",
    "bo = BOMain(\n",
    "    f_dummy,\n",
    "    bounds=bounds,\n",
    "    iterpts=0,\n",
    "    noise=0.01,  # Noise appropriate for the lignin data\n",
    "    kernel=\"rbf\",\n",
    "    acqfn_name=\"lcb\",\n",
    ")\n",
    "\n",
    "# Run one iteration with BOSS and get the results.\n",
    "res = bo.run(X, Y)\n",
    "\n",
    "# Extract the suggested new input variables, also known as the acquisition\n",
    "X_acq = res.get_next_acq(-1)\n",
    "\n",
    "# Update our dataframe and save to disk\n",
    "df.loc[len(df)] = np.append(X_acq.flatten(), np.nan)\n",
    "df.to_csv('lignin_data.csv')\n",
    "\n",
    "# Check that acquisition was successfully appended\n",
    "print(df.tail(1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d1664b5",
   "metadata": {
    "region_name": "md"
   },
   "source": [
    "The last row of the dataframe (and csv file) now contains the new acquisition,\n",
    "to continue the BO process we must measure the output variable, i.e. yield, and \n",
    "update the last row of our csv file with the measured value. We can rerun step 2.\n",
    "of this tutorial to get another acquisition and so on until convergence."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "region_name,-all",
   "main_language": "python",
   "notebook_metadata_filter": "-all"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
