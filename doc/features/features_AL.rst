.. _features_AL:

Active learning
===============

.. figure:: ../figures/features_AL.png
   :width: 100.0%

   Fig. 1: BOSS active learning features include a) objective function
   fitted with GP and refined with BO, b) acquisition cost to control
   sampling, and c) multi-task BO models.

BOSS builds surrogate models for the target properties with the
following active learning features:

- robust GP regression built on GPy with an easy input file
- several N-dimensional GP kernels (periodic/non-periodic)
- choice of priors on GP hyperparameters
- variety of acquisition functions
- batch acquisition strategies
- convergence-based stopping criteria
- support for symmetry considerations
- acquisition cost to control sampling
- gradient-assisted GPR and BO
- multi-task BO models for multi-fidelity studies, using data sources of varying cost and accuracy

