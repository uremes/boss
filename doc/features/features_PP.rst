.. _features_PP:

Postprocessing
===============

.. figure:: ../figures/features_PP.png
   :width: 100.0%

   Fig. 1: BOSS postprocessing features include a) hyperparameter tracking, b)
   local minima search, c) MEP detection, and d) Parento front analysis.

BOSS has readily available postprocessing features to extract data
from the surrogate model, for example:

- output and restart files
- acquisition data analysis
- global minima tracking with variance
- hyperparameter convergence tracking
- GPR model visualization
- multi-objective and Pareto front analysis
- automated detection of N-dimensional local minima
- minimum-energy path (MEP) extraction built on rapidly exploring random
  trees (RRT) and nudged elastic band (NEB)

