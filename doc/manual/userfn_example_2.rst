.. _userfn_example_2:

Example 2: Optimizing experiments
=================================

.. figure:: ../figures/userfn_example_2.png
   :width: 100.0%

   Fig. 1: 2D search with BOSS to maximize a lignin yield in experiments
   :sup:`1` with respect to the P-factor and temperature. The search starts
   with 5 initial points.  New points (green) are acquired in batches of 4.

In experiments, the optimized target property is typically the outcome
of the experiment. For example, in a chemical process, we may optimize the
yield of a reaction product (Fig. 1). The search parameters are then the
conditions of the experiment, for example the P-factor and temperature (T) for
a 2D search. In contrast to computational data acquisition (:ref:`Example
1<userfn_example_1>`), the BO search requires user intervention in each
iteration to evaluate the target function with experiment parameters given by
BOSS. In this case, the user function contains the following tasks:

a) Request the user to perform the experiment with parameters :math:`(P, T) = (x_1, x_2)`.

b) Prompt the user for experiment result :math:`f(x)`

c) Return :math:`f(x)` to BOSS

Besides the main target property, also other properties can be obtained from
the outcome of experiments. This data can be used to construct other surrogate
models with BOSS to optimize the other properties. It must be noted that these
additional models can be less converged than the initial model, since the data
points were selected by the acquisition function to optimize the main target
property.

[1] `J. Löfgren et al., ACS Sustainable Chem. Eng. 2022 10 (29), 9469 <https://pubs.acs.org/doi/full/10.1021/acssuschemeng.2c01895>`_
