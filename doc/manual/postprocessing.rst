BO postprocessing
====================

.. Much data is collected during the BO process. At each iteration, we monitor the convergence of the predicted global minimum points and compare them to data acquisitions, model fits are recorded via the hyperparameters and acquisition functions are stored. 

.. The accumulated data can be visualised afterwards by running postprocessing routines, as described :ref:`elsewhere<BOSS inputs and outputs>`.  Data in form of raw acsii files and graph is stored in the ``postprocessing`` folder.  BOSS postprocessing includes the following analysis:

Much data is collected during the iterative BO process. For example, the
predicted global minimum, GP hyperparameters, and acquisition
functions are stored at each iteration. The constructed surrogate
model can be data-mined by running advanced postprocessing routines,
as described in :ref:`BOSS inputs and outputs <BOSS inputs and
outputs>` and in the Tutorials section for :ref:`CLI
<postprocessing_cli>` and :ref:`API <postprocessing_py>`.

BOSS postprocessing data is stored as raw ascii files and graphs in
the ``postprocessing`` folder. By default, the postprocessing includes
the following analysis:

* Acquisition tracking and statistics

* Global minima tracking (:math:`x_{glmin}` locations) and convergence
  of :math:`x_{glmin}` and :math:`\mu_{glmin}`

* GP model hyperparameter tracking


.. raw:: html

   <h2>Data-mining the surrogate model</h2>

.. After the run, it is possible to reconstruct the GP surrogate
   models from the iterative process: these present a major resource
   and we have developed advanced postprocessing routines to extract
   further information such as:

After the BO process, the user can output data on the GP models via
BOSS postprocessing using the :ref:`pp_models <sec_pp_models>`
keyword. The postprocessing can be performed to all BO iterations
(default) or only specific iterations using the :ref:`pp_iters
<sec_pp_iters>` keyword. In addition to the default postprocessing
options above, BOSS has the following functionalities implemented (see
the :ref:`keywords <keywords_pp>` section for detailed instructions):

.. toctree::
   :maxdepth: 4

   pp_cross_sect
   pp_acq_func
   pp_truef
   pp_local_minima
   pp_mep
