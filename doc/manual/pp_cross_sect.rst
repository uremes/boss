.. _pp_cross_sect:

Model cross sections
====================

.. figure:: ../figures/pp_cross-sect.png
   :width: 100.0%

   Fig. 1: a) 1D and b) 2D cross sections of the GP surrogate model from BOSS
   postprocessing output. The models correspond to Tutorials 1 and 2 in the
   :ref:`Tutorials<tutorials>` section.

To visualize energy landscapes in high-dimensional GP models, the user
can output 1D curves or 2D cross sections (color maps) of specific
dimensions with given grid density using the :ref:`pp_model_slice
<sec_pp_model_slice>` keyword.  By default, the other dimensions are
set to the global minimum (:math:`x_{glmin}`) values. Alternatively,
they can be set to other fixed values using the :ref:`pp_var_defaults
<sec_pp_var_defaults>` keyword.
  
