.. _pp_mep:

Minimum Energy Paths (MEP)
==========================

.. figure:: ../figures/pp_mep.png
   :width: 100.0%

   Fig. 1: a) The lowest energy paths between local minima are found
   using the rapidly exporing random trees (RRT) method. b) The
   corresponding energy barriers are then detected with the nudged
   elastic band (NEB) method.

The GP model presents us with a relatively fast way of analyzing paths
on the domain :math:`\mathcal{X}`. Paths can be constructed by
multidimensional pathfinding methods, taking into account the whole
state space, instead of relying only on some select points (or images,
as they are sometimes known). BOSS has the rapidly exploring random
trees (RRT) method for finding paths with the lowest maximum energy.
This is used by first searching for the local minima (running
postprocessing with the ``pp_local_minima`` keyword), and using then
the command ``boss m <rst-file> <minima-file>``. It is a good idea to
first check the minima produced by the local minima search to avoid
unnecessary or duplicate minima from being given as input. The MEP
process can be controlled with the following keywords: :ref:`mep_maxe
<sec_mep_maxe>` sets the maximum energy and :ref:`mep_rrtsteps
<sec_mep_rrtsteps>` the maximum number of steps that the energy
threshold is increased in the RRT algorithm.  :ref:`mep_precision
<sec_mep_precision>` determines the precision of collision detection
and density of nudged elastic band (NEB) points, and
:ref:`mep_nebsteps<sec_mep_nebsteps>` sets the maximum number of
iterations in the NEB path optimization.
  

