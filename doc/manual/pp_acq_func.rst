.. _pp_acq_func:

Acquisition functions
=====================

.. figure:: ../figures/pp_acqfn.png
   :width: 100.0%

   Fig. 1: Acquisition function in the BOSS postprocessing output. A
   cost function is applied to region (x1, x2) < 240 to limit sampling
   there (red points) to avoid computational errors.
   
To monitor the selection of data points in the BO process, user can
output the acquisition functions (Fig. 1) using the :ref:`pp_acq_funcs
<sec_pp_acq_funcs>` keyword. This is particulary useful when
:ref:`cost functions <Cost functions>` are applied to the acquisition
function.
