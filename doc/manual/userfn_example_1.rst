.. _userfn_example_1:

Example 1: Atomistic structure search: molecule on surface
==========================================================

.. figure:: ../figures/userfn_example_1.png
   :width: 100.0%

   Fig. 1: a) Adsorbate search is performed in 6D for 3 rotations and
   3 translations of the molecule. b) Potential energy landscape with
   several local minima, which correspond to stable atomic structures.
   (Figures adapted from :sup:`1,2`).

For molecules on surfaces, the dimensionality of the search can be
reduced by considering the molecule and the surface as chemical
*building blocks*. The search can then be performed as a function of
rotations :math:`(\alpha, \beta, \gamma)` and translations :math:`(x,
y, z)` of the molecule (Fig. 1a), in this example in 6D for a single
adsorbate. The target property is the total potential energy of the
system, which we minimize to find the most probable structure and
other stable structures (Fig. 1b). Electronically accurate energy
sampling is typically performed with first-principles simulation
software. In this case, the user function performs the following
steps:

a) Generate the adsorbate structure with search coordinates (:math:`x_1 ... x_6`) given by BOSS

b) Run the energy calculation for this structure

c) Parse simulation output file to retrieve the energy :math:`f(x)` 

d) Return :math:`f(x)` to BOSS


In addition, the user function can extract and store other results
from the simulation output. Steps a--c can be carried out in Python,
Bash, or any other language, because it possible to write the search
coordinates :math:`x_1 ... x_6` and the computed energy :math:`f(x)`
to disk. When using Python scripts, we recommend to skip the disk i/o
and return variables directly to the function. The time-limiting step
in the entire BOSS run is b), which is typically executed in parallel
on HPC platforms. We recommend to save the simulation input/output
files of each iteration for further analysis and troubleshooting.

[1] `J. Järvi et al., Beilstein J. Nanotechnol. 11, 1577 (2020) <https://doi.org/10.3762/bjnano.11.140>`_

[2] `J. Järvi, "Structure search of molecular adsorbates with Bayesian
inference and density-functional theory", Doctoral thesis, Aalto
University (2023) <https://urn.fi/URN:ISBN:978-952-64-1113-2>`_ 
