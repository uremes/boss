.. _truef:

True functions
==============

.. figure:: ../figures/pp_truef.png
   :width: 100.0%

   Fig. 1: Postprocessing output shows the GP posterior mean (blue) in
   comparison to the true function (black).

In case of low-dimensional models (1D, 2D) and cheap energy
acquisitions, it can be viable to evaluate the accuracy of the
surrogate model in comparison to the true values of the objective
function (Fig 1). This can be done with the :ref:`pp_truef_npts
<sec_pp_truef_npts>` keyword, which sets the grid size of the computed
true function points. This approach is not tractable when energy
acquisitions are expensive, in which case it can be useful to output
the true function only in the predicted global minimum locations
:math:`x_{glmin}` using the :ref:`pp_truef_at_glmins
<sec_pp_truef_at_glmins>` keyword.
