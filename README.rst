BOSS
=========
`Bayesian Optimization Structure Search (BOSS) <https://sites.utu.fi/boss/>`_ is a general-purpose Bayesian Optimization code. It is designed to facilitate machine learning in computational and experimental natural sciences. See `research examples <https://sites.utu.fi/boss/research/>`_ for various applications of BOSS.

For a more detailed description of the code and tutorials, please consult the `user guide <https://cest-group.gitlab.io/boss>`_.

Installation
------------
BOSS is distributed as a PyPI package and can be installed using pip::

    python3 -m pip install aalto-boss

We recommend installing BOSS inside a virtual environment (``venv``, ``conda``...). If you are not using virtual environments, we recommend performing a user-installation instead::

    python3 -m pip install --user aalto-boss

Further instructions are provided in the user guide `installation <https://cest-group.gitlab.io/boss/installation.html>`_ section.

Usage
-----------
`Tutorials to get you started are available in our user guide <https://cest-group.gitlab.io/boss/tutorials.html>`_. Detailed descriptions of how BOSS operates are available in the `manual <https://cest-group.gitlab.io/boss/manual.html>`_.

Credits
-------
BOSS is under active development in the `Materials Informatics Laboratory` at the University of Turku and the `Computational Electronic Structure Theory (CEST) group <http://cest.aalto.fi/>`_ at Aalto University. For the full list of authors see `BOSS people  <https://sites.utu.fi/boss/people/>`_.


If you wish to use BOSS in your research, please use the `citation <https://sites.utu.fi/boss/about/>`_.


Issues and feature requests
---------------------------
It is strongly encouraged to submit bug reports, questions, and feature requests via the
`gitlab issue tracker <https://gitlab.com/cest-group/boss/issues>`_.
The BOSS development team can be contacted by email at milica.todorovic@utu.fi
