import unittest

import numpy as np
import pytest
from pytest import approx
from scipy.spatial.distance import euclidean

import boss.bo.factory as factory
from boss.bo.models.single_task import SingleTaskModel
from boss.settings import Settings


@pytest.fixture
def settings():
    keywords = {}
    x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
    y0 = np.array([[0], [-1], [0]])
    bounds = np.array([[0, 5], [0, 5], [0, 5]])
    dim = len(bounds)
    keywords["bounds"] = bounds
    keywords["kernel"] = ["rbf", "stdp", "stdp"]
    keywords["thetainit"] = [4, 3, 2, 1]
    keywords["periods"] = [5, 5, 5]
    keywords["thetabounds"] = [(0, 10)] * (dim + 1)
    keywords["thetaprior"] = "gamma"
    keywords["thetapriorpar"] = [[1, 1]] * (dim + 1)
    keywords["noise"] = 1e-10
    keywords["ynorm"] = False
    keywords["yrange"] = [np.min(y0), np.max(y0)]
    settings = Settings(keywords)
    return settings


@pytest.fixture
def toy_model(settings):
    """
    Initialize instance of toy_model class for each test
    """
    x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
    y0 = np.array([[0], [-1], [0]])
    kernel = factory.get_kernel(settings)
    model = SingleTaskModel(
        kernel,
        x0,
        y0,
        settings["noise"],
        settings["ynorm"],
    )
    return model


def test_add_data(toy_model):
    xnew = np.array([1, 1, 1])
    ynew = np.array([0.5])
    toy_model.add_data(xnew, ynew)
    assert toy_model.Y[-1] == approx(ynew)
    assert toy_model.X[-1] == approx(xnew)


def test_redefine_data(toy_model):
    xnew = np.array([[1, 1, 1], [2, 2, 2], [3, 3, 3]])
    ynew = np.array([[0.5], [1.5], [2.5]])
    toy_model.redefine_data(xnew, ynew)
    assert toy_model.Y == approx(ynew)
    assert toy_model.X == approx(xnew)


def test_set_get_unfixed_params(toy_model):
    params_in = [1, 2, 3, 4]
    toy_model.set_unfixed_params(params_in)
    params_out = toy_model.get_unfixed_params()
    assert params_in == approx(params_out)


def test_get_all_params(toy_model, settings):
    params = toy_model.get_all_params(include_fixed=False)
    param_vals = np.array(list(params.values()))
    expected_names = [
        "kernel.variance",
        "kernel.lengthscale",
        "kernel_1.lengthscale",
        "kernel_2.lengthscale",
    ]
    param_names = np.array(list(params.keys()))
    assert param_vals == approx(settings["thetainit"])
    assert all(param_names == expected_names)


def test_mu(toy_model):
    mu = toy_model.predict(toy_model.X)[0]
    assert mu == approx(toy_model.Y, abs=np.sqrt(toy_model.noise))


def test_predict_mean_grad(toy_model):
    x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
    y0 = np.array([[0], [0], [0]])
    model = SingleTaskModel(
        toy_model.kernel,
        x0,
        y0,
        toy_model.noise,
        toy_model.use_norm,
    )
    m, dmdx = model.predict_mean_grad(model.X, norm=False)
    assert m == approx(model.Y, abs=np.sqrt(toy_model.noise))
    dmdx_norm = np.linalg.norm(dmdx, axis=1)
    assert dmdx_norm == approx(np.zeros_like(dmdx_norm))


def test_get_best_xy(toy_model):
    xb = np.array([2, 2, 2])
    yb = np.array([-1])
    x, y = toy_model.get_best_xy()
    assert x == approx(xb)
    assert y == approx(yb)


def test_predict_mean_sd_grads(toy_model):
    x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
    y0 = np.array([[0], [0], [0]])
    model = SingleTaskModel(
        toy_model.kernel,
        x0,
        y0,
        toy_model.noise,
        toy_model.use_norm,
    )

    m, s, dmdx, dsdx = model.predict_mean_sd_grads(toy_model.X, norm=False)
    m, dmdx = model.predict_mean_grad(model.X, norm=False)
    assert m == approx(model.Y, abs=np.sqrt(toy_model.noise))
    dmdx_norm = np.linalg.norm(dmdx, axis=1)
    assert dmdx_norm == approx(np.zeros_like(dmdx_norm), abs=np.linalg.norm(dsdx, axis=1))
