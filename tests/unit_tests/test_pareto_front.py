import unittest

import numpy as np

from boss.pp.pf_main import PFMain


class TestParetoClass(unittest.TestCase):
    def setUp(self):
        self.mesh_size = 3
        self.bounds = np.array([[0, 2.0], [0, 2.0]])
        n_dim = np.shape(self.bounds)[0]
        self.n_mesh = (self.mesh_size,) * n_dim
        self.pf = PFMain(bounds=self.bounds, mesh_size=self.mesh_size)

    def test_position_coordinates(self):
        pf_1d = PFMain(mesh_size=self.mesh_size, bounds=np.array([[0.0, 3.0]]))
        coordinate_grid = np.array([[0.0, 1.5, 3.0]])
        value_to_test = pf_1d.position_coordinates(
            coordinate_grid=coordinate_grid, n_dim=1, values=[2]
        )[0]
        self.assertEqual(value_to_test, 3.0)


if __name__ == "__main__":
    unittest.main()
