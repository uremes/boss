import unittest
from unittest.mock import Mock

import pytest

try:  # GpyTorch Not dependency yet
    import gpytorch
except ImportError:
    pytest.skip("gpytorch not available", allow_module_level=True)

import numpy as np
from numpy.testing import assert_allclose, assert_array_less, assert_equal
from scipy.spatial.distance import euclidean

import boss.bo.kernels.torch.kernel_factory as kernel_factory
from boss.bo.models.torch.single_task import SingleTaskModel
from boss.settings import Settings


class ModelTest(unittest.TestCase):
    """
    Test cases for Model class
    """

    def setUp(self):
        """
        Initialize instance of Model class for each test
        """
        keywords = {}
        bounds = np.array([[0, 5], [0, 5], [0, 5]])
        self.dim = len(bounds)
        keywords["bounds"] = bounds
        keywords["kernel"] = ["rbf", "stdp", "stdp"]
        keywords["thetainit"] = [4, 3, 2, 1]
        keywords["periods"] = [5, 5, 5]
        keywords["thetabounds"] = [(0, 10)] * (self.dim + 1)
        keywords["thetaprior"] = "gamma"
        keywords["thetapriorpar"] = [[1, 1]] * (self.dim + 1)
        keywords["model_name"] = "torch"
        settings = Settings(keywords)

        self.kerntype = settings["kernel"]
        self.bounds = settings["bounds"]
        self.periods = settings["periods"]
        self.thetainit = settings["thetainit"]
        self.noise = 1e-3

        self.param_vals = np.hstack(
            (
                self.thetainit[:3],
                self.periods[1],
                self.thetainit[3],
                self.periods[2],
                self.noise,
            )
        )

        x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
        y0 = np.array([[0], [-1], [0]])
        settings["yrange"] = [np.min(y0), np.max(y0)]
        self.kernel = kernel_factory.select_kernel(settings)
        self.ynorm = False

        self.model = SingleTaskModel(
            self.kernel,
            x0,
            y0,
            self.noise,
            self.ynorm,
        )

    def test_add_data(self):
        xnew = np.array([1, 1, 1])
        ynew = np.array([0.5])

        self.model.add_data(xnew, ynew)
        assert_equal(self.model.Y[-1], ynew)
        assert_equal(self.model.X[-1], xnew)

    def test_redefine_data(self):
        xnew = np.array([[1, 1, 1], [2, 2, 2], [3, 3, 3]])
        ynew = np.array([[0.5], [1.5], [2.5]])

        self.model.redefine_data(xnew, ynew)

        assert_equal(self.model.Y, ynew)
        assert_equal(self.model.X, xnew)
    
    def test_get_all_params(self):
        params = self.model.get_all_params()
        assert_array_less(0, params)

    def test_set_get_unfixed_params(self):
        params_in = [1, 2, 3, 4]
        self.model.set_unfixed_params(params_in)
        params_out = [int(x) for x in self.model.get_unfixed_params()]

        assert_equal(params_in, params_out)

    def test_get_all_params(self):
        params = self.model.get_all_params(include_fixed=True)
        param_vals = np.array(list(params.values()))
        expected_names = [
            "variance",
            "kernel_0.lengthscale",
            "kernel_1.lengthscale",
            "kernel_1.period",
            "kernel_2.lengthscale",
            "kernel_2.period",
            "noise.variance",
        ]
        param_names = np.array(list(params.keys()))
        assert_equal(param_names, expected_names)
        assert_allclose(param_vals, self.param_vals, 0.6)

    def test_mu(self):
        mu = self.model.predict(self.model.X)[0]
        assert_allclose(mu, self.model.Y, atol=np.sqrt(self.noise))

    def test_predict_mean_grad(self):
        x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
        y0 = np.array([[0], [0], [0]])
        model = SingleTaskModel(
            self.kernel,
            x0,
            y0,
            self.noise,
            self.ynorm,
        )
        m, dmdx = model.predict_mean_grad(model.X, norm=False)

        assert_allclose(m, model.Y, atol=np.sqrt(self.noise))
        assert_allclose(dmdx, np.zeros(dmdx.shape), atol=np.sqrt(self.noise))

    def test_get_best_xy(self):
        xb = np.array([2, 2, 2])
        yb = np.array([-1])

        x, y = self.model.get_best_xy()
        assert_equal(x, xb)
        assert_equal(y, yb)

    def test_predict_mean_sd_grads(self):
        x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
        y0 = np.array([[0], [0], [0]])

        model = SingleTaskModel(
            self.kernel,
            x0,
            y0,
            self.noise,
            self.ynorm,
        )

        m, s, dmdx, dsdx = model.predict_mean_sd_grads(model.X, norm=False)

        for i in range(len(m)):
            with self.subTest(i=i):
                self.assertAlmostEqual(m[i, 0], model.Y[i, 0], delta=s[i, 0])
                self.assertAlmostEqual(
                    euclidean(dmdx[i], np.zeros(dmdx[i].shape)),
                    0,
                    delta=np.linalg.norm(dsdx[i]),
                )


if __name__ == "__main__":
    unittest.main()
