import unittest

import numpy as np
import pytest
from numpy.testing import assert_allclose, assert_array_equal, assert_equal
from scipy.spatial.distance import euclidean

import boss.bo.factory as factory
from boss.bo.models.gradient import GradientModel
from boss.settings import Settings


class GradientModelTest(unittest.TestCase):
    """
    Test cases for GradientModel class
    """

    def setUp(self):
        """
        Initialize instance of GradientModel class for each test
        """

        keywords = {}
        bounds = np.array([[0, 5], [0, 5], [0, 5]])
        self.dim = len(bounds)
        keywords["bounds"] = bounds
        keywords["kernel"] = ["rbf", "stdp", "stdp"]
        keywords["thetainit"] = [4, 3, 2, 1]
        keywords["periods"] = [5, 5, 5]
        keywords["thetabounds"] = [(0, 10)] * (self.dim + 1)
        keywords["thetaprior"] = "gamma"
        keywords["thetapriorpar"] = [[1, 1]] * (self.dim + 1)
        settings = Settings(keywords)

        self.kerntype = settings["kernel"]
        self.bounds = settings["bounds"]
        self.periods = settings["periods"]
        self.thetainit = settings["thetainit"]

        x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
        y0 = np.array([[0, -1, -1, -1], [-1, 0, 0, 0], [0, -2, -2, -2]])
        settings["yrange"] = [np.min(y0[:, 0]), np.max(y0[:, 0])]
        self.kernel = factory.get_kernel(settings)
        self.noise = 1e-10
        self.ynorm = False

        self.model = GradientModel(
            self.kernel,
            x0,
            y0,
            self.noise,
            self.ynorm,
        )

    def test_add_data(self):
        xnew = np.array([[1, 1, 1]])
        ynew = np.array([[0.5, 0.1, 0.2, 0.3]])

        self.model.add_data(xnew, ynew)
        assert_equal(self.model.Y[-1], np.squeeze(ynew))
        assert_equal(self.model.X[-1], np.squeeze(xnew))

    def test_redefine_data(self):
        xnew = np.array([[1, 1, 1], [2, 2, 2], [3, 3, 3]])
        ynew = np.array([[0.5, 1, 2, 3], [1.5, 1, 2, 3], [2.5, 1, 2, 3]])

        self.model.redefine_data(xnew, ynew)

        assert_equal(self.model.Y, ynew)
        assert_equal(self.model.X, xnew)

    def test_set_get_unfixed_params(self):
        params_in = [1, 2, 3, 4]

        self.model.set_unfixed_params(params_in)
        params_out = self.model.get_unfixed_params()
        assert_equal(params_in, params_out)

    def test_get_all_params(self):
        params = self.model.get_all_params(include_fixed=False)
        param_vals = np.array(list(params.values()))
        expected_names = [
            "kernel.variance",
            "kernel.lengthscale",
            "kernel_1.lengthscale",
            "kernel_2.lengthscale",
        ]
        param_names = np.array(list(params.keys()))
        assert_array_equal(param_vals, self.thetainit)
        assert_equal(param_names, expected_names)

    def test_mu(self):
        mu = self.model.predict(self.model.X)[0]

        assert_allclose(mu, self.model.Y[:, :1], atol=np.sqrt(self.noise))

    def test_predict_mean_grad(self):
        x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
        y0 = np.array([[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3]])

        model = GradientModel(
            self.kernel,
            x0,
            y0,
            self.noise,
            self.ynorm,
        )

        m, dmdx = model.predict_mean_grad(model.X, norm=False)

        assert_allclose(m, model.Y[:, :1], atol=np.sqrt(self.noise))
        assert_allclose(dmdx, model.Y[:, 1:, None], atol=np.sqrt(self.noise))

    def test_get_best_xy(self):
        xb = np.array([2, 2, 2])
        yb = np.array([-1])

        x, y = self.model.get_best_xy()

        assert_equal(x, xb)
        assert_equal(y, yb)

    def test_predict_mean_sd_grads(self):
        x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
        y0 = np.array([[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])
        model = GradientModel(
            self.kernel,
            x0,
            y0,
            self.noise,
            self.ynorm,
        )

        m, s, dmdx, dsdx = model.predict_mean_sd_grads(model.X, norm=False)

        for i in range(len(m)):
            with self.subTest(i=i):
                self.assertAlmostEqual(m[i, 0], model.Y[i, 0], delta=s[i, 0])
                self.assertAlmostEqual(
                    euclidean(dmdx[i], model.Y[i, 1:]),
                    0,
                    delta=np.linalg.norm(dsdx[i]),
                )


if __name__ == "__main__":
    unittest.main()
